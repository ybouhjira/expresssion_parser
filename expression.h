#include "arbre.h"

/*
 * Nom de la fonction: estOperateur
 * Entrées :
 *    char c : Un caractère
 * Sorties :
 *    int vrai : 1 si c est opérateur et 0 sinon
 * Description:
 *    Indique si un caractère est un opérateur
 */
int estOperateur(char c);

/*
 * Nom de la fonction: Suivant_expression
 * Entrées :
 *    char *expr: L'expression à lire
 *    int *pos: la position actuelle
 * Sorties :
 *    Noeud noeud : L'élément lu
 * Description:
 *    Retourne l'élément suivant dans l'éxpression qui est soit un nombre
 *  ou un opérateur, et avance pos jusqu'a la dernière case lue.
 */
Noeud Suivant_expression(char *expr, int* pos);

/*
 * Nom de la fonction: Construire_arbre
 * Entrées :
 *    char *expression : Une expression mathématique
 * Sorties :
 *    Arbre *arbre : L'arbre résultat
 * Description:
 *    Construit l'arbre représentant l'expression donnée
 */
Arbre* Construire_arbre(char *expression);

/*
 * Nom de la fonction: Calculer_expression
 * Entrées :
 *    Arbre *arbre : Arbre représentant une expression mathématique
 * Sorties :
 *    float resultat : Le résultat de l'évaluation de l'expression
 * Description:
 *    Calcule le résultat d'une expression a partir de l'arbre
 * correspondant
 *
 */
float Calculer_expression(Arbre *arbre, int *valide);
