TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    arbre.c \
    expression.c

HEADERS += \
    arbre.h \
    expression.h \
    validation.h

