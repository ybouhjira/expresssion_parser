#include "arbre.h"
#include <stdio.h>
#include <string.h>

void arbre_afficher_recursive(Arbre *arb, char *prefix, int isTail) {
  printf("%s%s", prefix, isTail ? "└──" : "├──");
  if(arb->valeur.type == FLOAT)
    printf("%f\n", arb->valeur.valeur.nombre);
  else
    printf("%c\n", arb->valeur.valeur.operateur);

  if(arb->gauche)
    {
      char gPrefix[100] = {0};
      strcat(gPrefix, prefix);
      strcat(gPrefix, isTail ? "    " : "│  ");
      arbre_afficher_recursive(arb->gauche, gPrefix, 0);
    }

  if(arb->droit)
    {
      char dPrefix[100] = {0};
      strcat(dPrefix, prefix);
      strcat(dPrefix, isTail ? "    " : "│  ");
      arbre_afficher_recursive(arb->droit, dPrefix, 1);
    }
}

void Afficher_arbre(Arbre *arb) {
  arbre_afficher_recursive(arb, "", 1);
}
