#ifndef VALIDATION_H
#define VALIDATION_H

#include "expression.h"

int attendre_nombre(char *expr, int *pos)
{

  // signe
  if(expr[*pos] == '+' || expr[*pos] == '-') (*pos)++;

  // partie entiere
  int nbrEntiers = 0;
  while(isdigit(expr[*pos]))
    {
      (*pos)++;
      nbrEntiers++;
    }
  if(nbrEntiers == 0) return 0;


  // point
  if(expr[*pos] == '.')
    {
      (*pos)++;

      // partie décimale
      int nbrDec = 0;
      while(isdigit(expr[*pos]))
        {
          (*pos)++;
          nbrDec++;
        }
      if(nbrDec == 0) return 0;
    }

  return 1;
}

int attendre_operateur(char *expr, int *pos)
{
  int valide = estOperateur(expr[*pos]);
  (*pos)++;
  return valide;
}

int expression_est_valide(char *expr)
{
  if(expr[0] == '\0') return 0; // expression vide est invalide

  int pos = 0, valide = 1;

  while(expr[pos] != '\0')
    {
      while(expr[pos] == ' ') pos++; //sauter les espaces

      // attendre nombre
      valide = attendre_nombre(expr, &pos);
      if(!valide || expr[pos] == '\0') break;

      while(expr[pos] == ' ') pos++; //sauter les espaces

      // attendre opérateur
      valide = attendre_operateur(expr, &pos);
      if(!valide) break;
      if(expr[pos] == '\0') valide = 0; // suivi d'un nombre

      while(expr[pos] == ' ') pos++; //sauter les espaces
    }

  return valide;
}

#endif // VALIDATION_H
