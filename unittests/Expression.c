#include <stdio.h>
#include <stdlib.h>

#include "CuTest/CuTest.h"

#include "../expression.h"

void Test_Suivant_expression(CuTest *tc)
{
  char *expression = "1+2*-3.5";
  int i = 0;

  Noeud noeud = Suivant_expression(expression, &i);
  CuAssertTrue(tc, noeud.type == FLOAT);
  CuAssertTrue(tc, noeud.valeur.nombre == 1);
  CuAssertIntEquals(tc, i, 1);

  noeud = Suivant_expression(expression, &i);
  CuAssertTrue(tc, noeud.type == CHAR);
  CuAssertTrue(tc, noeud.valeur.operateur == '+');
  CuAssertIntEquals(tc, i, 2);

  noeud = Suivant_expression(expression, &i);
  CuAssertTrue(tc, noeud.type == FLOAT);
  CuAssertTrue(tc, noeud.valeur.nombre == 2);
  CuAssertIntEquals(tc, i, 3);

  noeud = Suivant_expression(expression, &i);
  CuAssertTrue(tc, noeud.type == CHAR);
  CuAssertTrue(tc, noeud.valeur.operateur == '*');
  CuAssertIntEquals(tc, i, 4);

  noeud = Suivant_expression(expression, &i);
  CuAssertTrue(tc, noeud.type == FLOAT);
  CuAssertTrue(tc, noeud.valeur.nombre == -3.5);
  CuAssertIntEquals(tc, i, 8);

  CuAssertTrue(tc, expression[i] == '\0');

}

void Test_Construire_arbre(CuTest *tc)
{
  char *expression = "1+2*-3.5";
  Arbre *arbre = Construire_arbre(expression);

  /*
  Le Résultat doit etre :
     +
    / \
   1   *
      / \
     2 -3.5
  */

  CuAssertIntEquals(tc, arbre->valeur.type, CHAR);
  CuAssertIntEquals(tc, arbre->valeur.valeur.operateur, '+');

  CuAssertIntEquals(tc, arbre->gauche->valeur.type, FLOAT);
  CuAssertDblEquals(tc, arbre->gauche->valeur.valeur.nombre, 1, 0);

  CuAssertIntEquals(tc, arbre->droit->valeur.type, CHAR);
  CuAssertIntEquals(tc, arbre->droit->valeur.valeur.operateur, '*');

  arbre = arbre->droit;
  CuAssertIntEquals(tc, arbre->gauche->valeur.type, FLOAT);
  CuAssertDblEquals(tc, arbre->gauche->valeur.valeur.nombre, 2, 0);

  CuAssertIntEquals(tc, arbre->droit->valeur.type, FLOAT);
  CuAssertDblEquals(tc, arbre->droit->valeur.valeur.nombre, -3.5, 0);
}

void Test_Calculer_expression(CuTest *tc)
{
  char *expression = "1+2+3*4+5*6";
  float resultat = Calculer_expression(Construire_arbre(expression));
  CuAssertDblEquals(tc, 45, resultat, 0);

  expression = "11";
  resultat = Calculer_expression(Construire_arbre(expression));
  CuAssertDblEquals(tc, 11, resultat, 0);

  expression = "1+2++3";
  resultat = Calculer_expression(Construire_arbre(expression));
  CuAssertDblEquals(tc, 6, resultat, 0);
}

CuSuite* Expression_get_suite()
{
  CuSuite *suite = CuSuiteNew();
  SUITE_ADD_TEST(suite, Test_Suivant_expression);
  SUITE_ADD_TEST(suite, Test_Construire_arbre);
  SUITE_ADD_TEST(suite, Test_Calculer_expression);
  return suite;
}
