#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "arbre.h"
#include "expression.h"
#include "validation.h"

// supprime les espaces d'une chaine de caractères
void rm_space(char *str) {

    char *src, *dst;
    for (src = dst = str; *src != '\0'; src++) {
        *dst = *src;
        if (*dst != ' ') dst++;
    }
    *dst = '\0';
}

int main()
{
  char expression[1000];

  // Lecture de l'expression

  printf("Entrez une expression : ");
  scanf("%[^\n]s", expression);

  printf("%s\n", expression);

  if(expression_est_valide(expression))
    {
      rm_space(expression);

      // Construction de l'arbre
      Arbre *arbre = Construire_arbre(expression);
      printf("Arbre de l'expression : \n");
      Afficher_arbre(arbre);

      // Calcule du résultat :
      int valide = 1;
      float res = Calculer_expression(arbre, &valide);


      if(valide)
        {
          printf("Résultat : %.2f\n\n", res);
        }
      else
        {
          printf("division par 0\n");
        }
    }
  else
    {
      printf("L'expression saisie est invalide\n");
    }



  return 0;
}
